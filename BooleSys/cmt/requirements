package BooleSys
version           v30r2

branches cmt doc
 
#=======================================================
# packages included in the BOOLE project
#=======================================================

# Boole application                    # Maintainer  
use Boole               v*    Digi     # Marco Cattaneo
use BooleUMC            v*    Digi     # Tomasz Skwarnicki
use DigiAlg             v*    Digi     # Marco Cattaneo

# Bcm
use BcmDigi             v*    Bcm      # Magnus Lieng

# Calorimeter
use CaloDigit           v*    Calo     # Olivier Deschamps
use CaloMoniDigi        v*    Calo     # Olivier Deschamps

# Fibre Tracker
use FTDigitisation      v*    FT       # Eric Cogneras

# Herschel 
use HCDigitisation      v*    HC       # Heinrich Schindler

# Muon
use MuonAlgs            v*    Muon     # Alessia Satta
use MuonBackground      v*    Muon     # Alessia Satta
use MuonMoniDigi        v*    Muon     # Alessio Sarti

# Outer Tracker
use OTSimulation        v*    OT       # Alexandr Kozlinskiy

# Rich
use RichDigiQC          v*    Rich     # Chris Jones
use RichDigiSys         v*    Rich     # Chris Jones
use RichMCAssociators   v*    Rich     # Chris Jones
use RichReadout         v*    Rich     # Chris Jones

# Silicon Tracker
use STDigiAlgorithms    v*    ST       # Jeroen van Tilburg

# VP
use VPDigitisation      v*    VP       # Heinrich Schindler

# Allow the generation of the SAM QMTest
apply_pattern QMTest

# Declare this as a container package
apply_pattern container_package

# Allow the generation of QMTest summary 
apply_pattern QMTestSummarize

# Packages from other projects updated temporarily in this release
