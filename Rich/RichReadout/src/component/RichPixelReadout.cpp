
#include "RichPixelReadout.h"

#include "RichShape.h"
#include "RichFrontEndDigitiser.h"

using namespace Rich::MC::Digi;

RichPixelReadout::RichPixelReadout()
  :  m_shape    ( new RichShape(25,2.7) ),
     m_frontEnd ( new RichFrontEndDigitiser() )
{

  //const double peakTime    = 25.0;
  //const double calib       = 4.42;
  //const double threshold   = 8.8;
  //m_shape      = new RichShape( peakTime, 2.7 );
  //m_frontEnd   = new RichFrontEndDigitiser( threshold * m_sigmaElecNoise, calib );

}

RichPixelReadout::~RichPixelReadout()
{
  delete m_shape;
  delete m_frontEnd;
}
